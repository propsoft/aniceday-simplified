# A Nice Day - Simplified

## Notes

Here is a much simplified version of the 'A Nice Day' application exercise.

No use of Webpack, Vuetify or any other 3rd party frameworks or libraries outside of Vue itself.

## Project setup

```
simply open the index.html file in a browser
```
